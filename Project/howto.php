<?php include './configs/autoload.php'; ?>
<?php load('handles/cart') ?>
<!DOCTYPE HTML>
<html>
    <head>
        <title></title>
        <?php load('styles') ?>
    </head>
    <body>
        <div class="wrap">
            <div class="container">
                <?php load('header', 'howto') ?>
                <div class="content">
                    <div class="content_box">
                        <?php load('pages/howto') ?>
                        <?php load('footer') ?>
                    </div>
                </div>
            </div>
        </div>
        <?php load('scripts') ?>
    </body>
</html>
