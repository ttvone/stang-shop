<?php

$cache_time = 2000;
require_once '../configs/autoload.php';
if (!empty($id = get_input('id')) || !empty($prod_id = get_input('prod_id'))) {
    $product_image = null;
    if (!empty($prod_id)) {
        $product_image = database_select_row("select * from product_image where prod_id=$prod_id");
    } else {
        $product_image = database_select_row("select * from product_image where image_id=$id");
    }
    header('Pragma: public');
    header('Cache-Control: max-age=' . $cache_time);
    header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', time() + $cache_time));
    if (!empty($product_image)) {
        header('Content-Type: ' . $product_image['image_type']);
        exit($product_image['image_content']);
    } else {
        header('Content-Type: image/png');
        exit(file_get_contents('./no-image.png'));
    }
}

