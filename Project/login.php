<?php include './configs/autoload.php'; ?>
<?php include './handles/login.php'; ?>
<!DOCTYPE HTML>
<html>
    <head>
        <title></title>
        <?php load('styles') ?>
    </head>
    <body>
        <div class="wrap">
            <div class="container">
                <?php load('header', 'login') ?>
                <div class="content">
                    <div class="content_box">
                        <?php load('pages/login') ?>
                        <?php load('footer') ?>
                    </div>
                </div>
            </div>
        </div>
        <?php load('scripts') ?>
    </body>
</html>
