<?php
include './configs/autoload.php';
$admin_page = '';
switch (get_input('site')) {
    case 'category':
        load('handles/category');
        $admin_page = 'category';
        break;
    case 'product':
        load('handles/product');
        $admin_page = 'product';
        break;
    case 'order':
        load('handles/order');
        $admin_page = 'order';
        break;
    case 'report':
        load('handles/report');
        $admin_page = 'report';
        break;
    case 'stock':
        load('handles/stock');
        $admin_page = 'stock';
        break;
    case 'payment':
        load('handles/payment');
        $admin_page = 'payment';
        break;
    default :
        $admin_page = 'index';
        break;
}
?>
<!DOCTYPE HTML>
<html>
    <head>
        <title>ระบบหลังบ้าน : Administrator</title>
        <?php load('styles') ?>
    </head>
    <body>
        <div class="wrap">
            <div class="container">
                <?php load('header', 'admin') ?>
                <div class="content">
                    <div class="content_box">
                        <div id="administrator" class="administrator">
                            <?php load_admin($admin_page) ?>
                        </div>
                        <?php load('footer') ?>
                    </div>
                </div>
            </div>
        </div>
        <?php load('scripts') ?>
    </body>
</html>
