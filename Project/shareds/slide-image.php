<div class="grid_1">
    <div class="col-md-3 banner_left">
        <img src="<?= get_url('/assets/images/pic1.png') ?>" class="img-responsive" alt=""/>
        <div class="banner_desc">
            <h1>เสื้อยืดสุดเท่</h1>
            <h2>Roundcheck T-Shirt</h2>
            <h5>฿1250 <small>เท่านั้น</small></h5>
            <a href="#" class="btn1 btn4 btn-1 btn1-1b">หยิบใส่ตระกร้า</a>
        </div>
    </div>
    <div class="col-md-9 banner_right">
        <!-- FlexSlider -->
        <section class="slider">
            <div class="flexslider">
                <ul class="slides">
                    <li><img src="<?= get_url('/assets/images/banner.jpg') ?>" alt=""/></li>
                    <li><img src="<?= get_url('/assets/images/banner1.jpg') ?>" alt=""/></li>
                </ul>
            </div>
        </section>
        <!-- FlexSlider -->
    </div>
    <div class="clearfix"></div>
</div>
