<?php
// 5 อันดับ สินค้าที่คนดีเยอะสุด
$top_5_products = database_select_all('select * from product order by prod_view_count desc limit 0, 5');
?>
<div class="footer_top hidden-print">
    <div class="span_of_4">
        <div class="col-md-3 box_4">
            <h4>ร้านค้า</h4>
            <ul class="f_nav">
                <li><a href="<?= get_url('/') ?>">หน้าแรก</a></li>
                <li><a href="<?= get_url('/howto.php') ?>">วิธีการสั่งซื้อ</a></li>
                <li><a href="<?= get_url('/contact.php') ?>">ติดต่อเรา</a></li>
            </ul>
        </div>
        <div class="col-md-3 box_4">
            <h4>ช่วยเหลือ</h4>
            <ul class="f_nav">
                <li>
                    <a href="#" onclick="return go_to_orders('<?= get_url('cart.php?site=order-invoice') ?>')">
                        ค้นหารายการสั่งซื้อของคุณ
                    </a>
                </li>
                <li><a href="<?= get_url('/payment.php') ?>">แจ้งชำระเงินค่าสินค้า</a></li>
                <li>
                    <a href="mailTo:example@hotmail.com">ส่งเมล์มาสอบถามข้อมูลจากเรา</a>
                </li>
            </ul>
        </div>
        <div class="col-md-3 box_4">
            <h4>บัญชี</h4>
            <ul class="f_nav">
                <li><a href="<?= get_url('/login.php') ?>">เข้าสู่ระบบ</a></li>
                <li><a href="<?= get_url('/cart.php') ?>?>">ตระกร้าสินค้า</a></li>
                <li><a href="#" onclick="return go_to_orders('<?= get_url('cart.php?site=order-invoice') ?>')">ประวัติการสั่งซื้อ</a></li>
            </ul>
        </div>
        <div class="col-md-3 box_4">
            <h4>ยอดนิยม</h4>
            <ul class="f_nav">
                <?php foreach ($top_5_products as $item) : ?>
                    <li><a href="<?= get_url('/single.php', ['prod_id' => $item['prod_id']]) ?>"><?= $item['prod_name'] ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- start span_of_2 -->
    <div class="span_of_2">
        <div class="span1_of_2">
            <h5><a href="#">ติดต่อเรา<span> &gt;</span> </a> </h5>
            <p>(หรือ) โทร: +22-34-2458793</p>
        </div>
        <div class="span1_of_2">
            <h5>ติดตามพวกเรา</h5>
            <div class="social-icons">
                <ul>
                    <li><a href="#" target="_blank"></a></li>
                    <li><a href="#" target="_blank"></a></li>
                    <li><a href="#" target="_blank"></a></li>
                    <li><a href="#" target="_blank"></a></li>
                    <li class="last2"><a href="#" target="_blank"></a></li>
                </ul>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="copy">
        <p>© 2017 All Rights Reseverd Steak shop</p>
    </div>
</div>
