<!--Load meta tag-->
<META NAME="ROBOTS" CONTENT="NOINDEX, FOLLOW">
<META NAME="ROBOTS" CONTENT="INDEX, NOFOLLOW">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<!--Load Stylesheet-->
<link href="https://fonts.googleapis.com/css?family=Pattaya" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Taviraj:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link href="<?= get_url('/assets/css/bootstrap.min.css') ?>" rel='stylesheet' />
<link href="<?= get_url('/assets/css/megamenu.css') ?>" rel="stylesheet" />
<link href="<?= get_url('/assets/css/flexslider.css') ?>" rel='stylesheet' />
<link href="<?= get_url('/assets/css/etalage.css') ?>" rel="stylesheet" />
<link href="<?= get_url('/assets/css/style.css') ?>" rel='stylesheet' />
<link href="<?= get_url('/assets/frontend-styles.css') ?>" rel='stylesheet' />
<link href="<?= get_url('/assets/backend-styles.css') ?>" rel='stylesheet' />