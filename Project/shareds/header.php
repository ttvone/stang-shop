<!--Nave Header-->
<div class="header_top hidden-print">
    <div class="col-sm-9 h_menu4">
        <ul class="megamenu skyblue">
            <li class="grid <?= $model == 'home' ? 'active' : '' ?>"><a class="color8" href="<?= get_url('/') ?>">หน้าหลัก</a></li>
            <li class="<?= $model == 'howto' ? 'active' : '' ?>">
                <a class="color4" href="<?= get_url('/howto.php') ?>">วิธีการซื้อสินค้า</a>
            </li>
            <li class="<?= $model == 'contact' ? 'active' : '' ?>"><a class="color6" href="<?= get_url('/contact.php') ?>">ติดต่อเรา</a></li>
            <?php if (user_authentication()) : ?>
                <li class="<?= $model == 'admin' ? 'active' : '' ?>">
                    <a class="color10" href="<?= get_url('/admin.php') ?>">
                        <i class="glyphicon glyphicon-lock"></i> ผู้ดูแลระบบ
                    </a>
                </li>
            <?php endif; ?>
        </ul>
    </div>
    <div class="col-sm-3 header-top-right text-right">
        <div class="register-info">
            <ul>
                <?php if (user_authentication()) : ?>
                    <li class="<?= $model == 'login' ? 'active' : '' ?>"><a href="<?= get_url('/logout.php') ?>">ออกจากระบบ</a></li>
                <?php else: ?>
                    <li class="<?= $model == 'login' ? 'active' : '' ?>"><a href="<?= get_url('/login.php') ?>">เข้าสู่ระบบ</a></li>
                <?php endif; ?>
            </ul>
        </div>
        <div class="clearfix"> </div>
    </div>
    <div class="clearfix"> </div>
</div>

<!--Logo header-->
<div class="header_bootm">
    <div class="col-sm-4 span_1">
        <a class="logo">
            Stang shop
        </a>
    </div>
    <div class="col-sm-8 row_2 hidden-print">
        <div class="header_bottom_right">
            <div class="search">
                <form method="get" action="<?= get_url('/') ?>">
                    <input type="text" placeholder="ค้นหาสินค้าในระบบ" name="search">
                    <input type="submit" value="">
                </form>
            </div>
            <ul class="bag">
                <a href="<?= get_url('/cart.php') ?>">
                    <li class="bag_right">
                        <p class="text-center">
                            <i class="glyphicon glyphicon-shopping-cart" style="font-size: 13px;"></i>
                            ฿<?= number_format(get_cart_amount(), 2) ?>
                        </p>
                    </li>
                </a>
                <div class="clearfix"> </div>
            </ul>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
