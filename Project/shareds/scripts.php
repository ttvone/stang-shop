<script type="text/javascript" src="<?= get_url('/assets/js/jquery-1.11.1.min.js') ?>"></script>
<script type="text/javascript" src="<?= get_url('/assets/js/megamenu.js') ?>"></script>
<script type="text/javascript" src="<?= get_url('/assets/js/jquery.flexslider.js') ?>"></script>
<script type="text/javascript" src="<?= get_url('/assets/js/jquery.etalage.min.js') ?>"></script>
<script type="text/javascript" src="<?= get_url('/assets/frontend-scripts.js') ?>"></script>
<script type="text/javascript" src="<?= get_url('/assets/backend-scripts.js') ?>"></script>