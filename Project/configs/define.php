<?php

// root file ที่อยู่
define('WEB_ROOT', $_SERVER['DOCUMENT_ROOT']);

// ตั่งค่าแสดงผล จำนวนรายการที่แสดงรายการ
define('PAGE_MAX', 10);

// ตั้งค่า Base url
define('BASE_URL', 'http://localhost:9000');

// ตั่งค่า เชื่อมต่อฐานข้อมูล
const DB = [
    'host' => 'devthailand.com',
    'database' => 'steak_shop',
    'username' => 'steak_shop',
    'password' => 'steak_shop'
];
