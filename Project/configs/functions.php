<?php

function load($file, $model = null) {
    $content = WEB_ROOT . '/' . $file . '.php';
    $page = WEB_ROOT . '/pages/' . $file . '.php';
    $shared = WEB_ROOT . '/shareds/' . $file . '.php';
    // content file
    if (file_exists($content)) {
        require_once ($content);
    }
    // page file
    else if (file_exists($page)) {
        require_once ($page);
    }
    // shared file
    else if (file_exists($shared)) {
        require_once ($shared);
    }
}

function load_admin($file, $model = []) {
    load('pages_admin/' . $file, $model);
}

function get_site($site, $params = []) {
    return get_url($_SERVER['PHP_SELF'] . '?site=' . $site, $params);
}

function get_url($url = '/', $params = []) {
    $url_return = $url[0] != '/' ? '/' . $url : $url;
    $param_return = '';
    $index = 0;
    foreach ($params as $key => $val) {
        if ($index == 0 && !strpos($url_return, '?')) {
            $param_return .= '?' . $key . '=' . $val;
        } else {
            $param_return .= '&' . $key . '=' . $val;
        }
        $index++;
    }
    return BASE_URL . $url_return . $param_return;
}

function get_input($name = null) {
    if (empty($name))
        return $_REQUEST;
    if (isset($_REQUEST[$name])) {
        return trim($_REQUEST[$name]);
    }
    return null;
}

function get_product_into_cart($product_id) {
    return get_url('cart.php', ['product_into_cart' => $product_id]);
}

function set_value($name, $value = '') {
    return get_input($name) ?: $value;
}

function alert($str, $status = 400) {
    header('Content-Type: text/html; charset=utf-8', true, $status);
    echo "<script>alert('$str');</script>";
}

function alert_error($title, $content, $goback = null) {
    $title = addslashes($title);
    $content = addslashes($content);
    $location = "/error.php?title=$title&content=$content";
    if ($goback !== null) {
        $location .= '&goback=' . $goback;
    }
    redirect($location);
}

function redirect($location, $clentRedirect = true) {
    if ($clentRedirect) {
        exit("<script>location.href='$location';</script>");
    }
    header("Location: $location");
    exit();
}

function get_back() {
    return $_SERVER['HTTP_REFERER'];
}

function user_authentication($value = null, $clear = false) {
    $key_session = 'authen_session';
    if ($clear) {
        unset($_SESSION[$key_session]);
        return;
    }
    if (!empty($value)) {
        $_SESSION[$key_session] = $value;
        return $value;
    }
    return isset($_SESSION[$key_session]) ? $_SESSION[$key_session] : null;
}

function file_upload_errors($code) {
    switch ($code) {
        case UPLOAD_ERR_INI_SIZE:
            $message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
            break;
        case UPLOAD_ERR_FORM_SIZE:
            $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
            break;
        case UPLOAD_ERR_PARTIAL:
            $message = "The uploaded file was only partially uploaded";
            break;
        case UPLOAD_ERR_NO_FILE:
            $message = "No file was uploaded";
            break;
        case UPLOAD_ERR_NO_TMP_DIR:
            $message = "Missing a temporary folder";
            break;
        case UPLOAD_ERR_CANT_WRITE:
            $message = "Failed to write file to disk";
            break;
        case UPLOAD_ERR_EXTENSION:
            $message = "File upload stopped by extension";
            break;

        default:
            $message = "Unknown upload error";
            break;
    }
    return $message;
}

function pagination_template($site_name, $pagination) {
    echo '<ul class="pagination">';
    $pages = ceil($pagination['rows'] / $pagination['page']);
    for ($index = 0; $index < $pages; $index++) {
        $page = $index + 1;
        $paging = get_input('paging') ?: 1;
        $active_class = $paging == $page ? 'active' : '';
        $href = $site_name == null ? get_url('/', ['paging' => $page]) : get_site($site_name, ['paging' => $page]);

        echo "<li class='$active_class'>";
        echo "<a href='$href'>$page</a>";
        echo "</li>";
    }
    echo '</ul>';
}

function search_object(array $objects, $key, $value, $push_array = null, $show_index = false) {
    $objected = $push_array;
    $index = 0;
    foreach ($objects as $item) {
        if ($item[$key] == $value) {
            if (!is_array($objected)) {
                $objected = $item;
                break;
            }
            $objected[] = $item;
        }
        $index = $index + 1;
    }
    return $show_index ? ['index' => $index, 'data' => $objected] : $objected;
}

function select_object_to_single_array(array $objects, $key) {
    $objected = [];
    foreach ($objects as $item) {
        if (!empty($item[$key])) {
            $objected[] = $item[$key];
        }
    }
    return $objected;
}

// สร้าง Session เก็บรหัสสินค้า
function cart_store($product_id = 0, $amount = 1, $isEdit = false) {
    $session_key = 'CART_SESSON';
    // ลบข้อมูลออกจาก Store
    if ($product_id === -1) {
        unset($_SESSION[$session_key]);
    }
    // ถ้ามีการส่งสินคาเข้ามา
    else if (!empty($product_id)) {
        // ถ้ามีสินค้าใน Store แล้ว
        if (isset($_SESSION[$session_key]) && !empty($_SESSION[$session_key])) {
            // สินค้าที่อยู่ใน Store ต้องเป็น Array
            if (is_array($_SESSION[$session_key])) {

                // ถ้า $amount = -1 แสดงว่าต้องการจะลบสินค้า
                if ($amount === -1) {
                    $cart_store = [];
                    foreach ($_SESSION[$session_key] as $item) {
                        if ($item['prod_id'] !== $product_id) {
                            $cart_store[] = $item;
                        }
                    }
                    $_SESSION[$session_key] = $cart_store;
                }

                // ถ้า $amount != -1 แสดงว่าเป็นการเพิ่มหรือแก้ไขสินค้า
                else {
                    $product = search_object($_SESSION[$session_key], 'prod_id', $product_id, null, true);
                    // เพิ่มสินค้าเข้าไปใหม่
                    if (empty($product['data'])) {
                        $_SESSION[$session_key][] = [
                            'prod_id' => $product_id,
                            'amount' => $amount
                        ];
                    }
                    // แก้ไขสินค้าเดิม
                    else {
                        if ($isEdit) {
                            $_SESSION[$session_key][$product['index']]['amount'] = $amount;
                        } else {
                            $_SESSION[$session_key][$product['index']]['amount'] += $amount;
                        }
                    }
                }
            }
        }
        // ถ้ายังไม่มีสินค้าใน Store
        else {
            $_SESSION[$session_key] = [];
            $_SESSION[$session_key][] = [
                'prod_id' => $product_id,
                'amount' => $amount
            ];
        }
    }
    // return สินค้าที่อยู่ใน Store ออกไป
    return isset($_SESSION[$session_key]) ? $_SESSION[$session_key] : [];
}

function get_cart_amount($show_detail = false) {
    $store = cart_store();
    $store_ids = select_object_to_single_array($store, 'prod_id');
    $total = 0;
    $count = 0;
    $products = [];
    if (count($store_ids) > 0) {
        $prod_ids = join(',', $store_ids);
        $products_query = database_select_all("select * from product where prod_id in ($prod_ids)");
        foreach ($products_query as $item) {
            $product = search_object($store, 'prod_id', $item['prod_id']);
            if (!empty($product)) {
                $total += $item['prod_price'] * $product['amount'];
                $item['cart_amount'] = $product['amount'];
                $products[] = $item;
                $count++;
            }
        }
    }
    return !$show_detail ? $total : [
        'total' => $total,
        'count' => $count,
        'products' => $products,
        'tax' => 0, //($total * 7 / 100),
        'shipping' => 50
    ];
}

// Oreder = ENUM('new', 'pending', 'success');
// Shipping = ENUM('pending', 'packing', 'success')
function get_order_status($status, $type = 'order') {
    $status_return = '';
    switch ($type) {
        case 'order':
            switch ($status) {
                case 'new':
                    $status_return = 'รายการสั่งซื้อใหม่';
                    break;
                case 'pending':
                    $status_return = 'รอดำเนินการ';
                    break;
                case 'cancel':
                    $status_return = 'ยกเลิกการสั่งซื้อ';
                    break;
                case 'success':
                    $status_return = 'ทำรายการเสร็จสิ้น';
                    break;
            }
            break;
        case 'shipping':
            switch ($status) {
                case 'pending':
                    $status_return = 'รอดำเนินการ';
                    break;
                case 'packing':
                    $status_return = 'กำลังจัดส่ง';
                    break;
                case 'success':
                    $status_return = 'จัดส่งแล้ว';
                    break;
            }
            break;
        case 'payment':
            switch ($status) {
                case 'pending':
                    $status_return = 'รอชำระเงิน';
                    break;
                case 'paid':
                    $status_return = 'ชำระเงินแล้ว';
                    break;
            }
            break;
    }
    return $status_return;
}