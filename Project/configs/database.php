<?php

$database = new mysqli(DB['host'], DB['username'], DB['password'], DB['database']);
if (mysqli_connect_error()) {
    die('Database is error : ' . mysqli_connect_error());
}

// ฟังก์ชั่นหลัก
function database() {
    global $database;
    return $database;
}

// Query ข้อมูล
function database_query($sql) {
    $query = mysqli_query(database(), $sql);
    if (!$query) {
        @header("HTTP/1.0 400 " . database_error());
        if (strpos($sql, 'select') === 0)
            echo ('<div class="alert alert-danger">' . database_error() . '</div>');
    }
    return $query;
}

// Encode field ข้อมูลแบบ ปลอดภัย
function database_escape_string($field) {
    return mysqli_escape_string(database(), $field);
}

// Select ข้อมูลทั้งหมด
function database_select_all($sql) {
    return mysqli_fetch_all(database_query($sql), MYSQLI_ASSOC);
}

// Select ข้อมูลเดียว
function database_select_row($sql) {
    return mysqli_fetch_assoc(database_query($sql));
}

// Select จำนวนแถว
function database_num_rows($sql) {
    return mysqli_num_rows(database_query($sql));
}

// select id ที่ insert ล่าสุด
function database_insert_id() {
    return mysqli_insert_id(database());
}

// ตรวจสอบการ Error
function database_error() {
    return mysqli_error(database());
}

// ทำตัวแบ่งหน้า
function get_pagination($sql, $page) {
    $page_end = $page;
    $page_start = $page_end * (set_value('paging', 1) - 1);
    $rows = database_num_rows($sql);
    $sql = $sql . " limit $page_start, $page_end";
    return [
        'items' => database_select_all($sql),
        'rows' => $rows,
        'page' => $page_end
    ];
}
