<?php
// ตรวจสอบว่ามาหน้า detail หรือไม่
if (get_input('product_detail')) {
    $prod_id = get_input('product_detail');
    $product = get_product_by_id($prod_id);
    if (empty($product)) {
        alert_error(404, 'ไม่มีข้อมูลในระบบ');
    }
}
?>

<h3>รายละเอียดสินค้า</h3>
<br>
<div class="form-group">
    <div class="pull-right">
        <a href="<?= get_site('product', ['page' => 'form']) ?>" class="btn btn-info">เพิ่มข้อมูลใหม่</a>
        <a href="<?= get_back() ?>" class="btn btn-default">กลับไปหน้ารายการ</a>
    </div>
    <h4>
        แสดงรายละเอียดข้อมูล "<?= $product['prod_name'] ?>"
    </h4>
    <div class="clearfix"></div>
</div>
<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <td style="width: 150px;">รหัสสินค้า :</td>
            <td><?= $product['prod_id'] ?></td>
        </tr>
        <tr>
            <td>ชื่อสินค้า :</td>
            <td><?= $product['prod_name'] ?></td>
        </tr>
        <tr>
            <td>ประเภทสินค้า :</td>
            <td><?= $product['cate_name'] ?></td>
        </tr>
        <tr>
            <td>ราคาสินค้า :</td>
            <td><?= $product['prod_price'] ?> บาท</td>
        </tr>
        <tr>
            <td>จำนวนที่เหลือ :</td>
            <td><?= $product['prod_amount'] ?> ชิ้น</td>
        </tr>
        <tr>
            <td>รายละเอียดอื่นๆ :</td>
            <td><?= $product['prod_detail'] ?: 'ไม่มีข้อมูล' ?></td>
        </tr>
        <tr>
            <td>ภาพสินค้า :</td>
            <td>
                <?php foreach (get_product_images($prod_id) as $img) : ?>
                    <a href="<?= get_url("/images?id={$img['image_id']}") ?>" target="_blank" style="text-decoration: none;">
                        <img src="<?= get_url("/images?id={$img['image_id']}") ?>" class="product-image-detail img-thumbnail" alt="<?= $img['image_name'] ?>"/>
                    </a>
                <?php endforeach; ?>
            </td>
        </tr>
        <tr>
            <td>วันที่ :</td>
            <td><?= $product['prod_created'] ?></td>
        </tr>
    </table>
</div>