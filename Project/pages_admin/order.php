<?php
$orders = get_orders();
?>

<h3>ข้อมูลการสั่งซื้อของลูกค้า</h3>
<br>
<div class="form-group">
    <div class="pull-right">
        <a href="<?= get_url('/admin.php') ?>" class="btn btn-default">กลับไปหน้าหลัก</a>
    </div>
    <h4>
        ตาราง แสดงรายการข้อมูลการสั่งซื้อของลูกค้า
    </h4>
</div>
<br>
<div class="form-group">
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>เลขที่สั่งซื้อ</th>
                    <th>สถานะการสั่งซื้อ</th>
                    <th>วันที่สั่งซื้อ</th>
                    <th>ใบเสร็จชำระเงิน</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($orders as $item) : ?>
                    <tr>
                        <td>
                            <a href="<?= get_url('/cart.php', ['site' => 'order-invoice', 'id' => $item['ord_number']]) ?>">
                                <u><?= $item['ord_number'] ?></u>
                            </a>
                        </td>
                        <td>
                            <form method="post">
                                <div class="show-status box-status">
                                    <?= get_order_status($item['ord_status'], 'order') ?>
                                    <a href="#" data-value='<?= $item['ord_status'] ?>' class="label label-default" onclick="return update_order_status(this, true)">
                                        <i class="glyphicon glyphicon-edit"></i>
                                    </a>
                                    <?php if($item['ord_status'] != 'success') : ?>
                                        <p class='text-small'>
                                            <a href="<?= get_site('payment') ?>">
                                                <u><?= empty($item['pay_amount']) ? '' : 'ลูกค้าแจ้งชำระเงินแล้ว' ?></u>
                                            </a>
                                        </p>
                                    <?php endif; ?>
                                </div>
                                <div class="update-status box-status" style="display: none;">
                                    <select name="ord_status" class="select_status">
                                        <option value="new">รายการสั่งซื้อใหม่</option>
                                        <option value="pending">รอดำเนินการ</option>
                                        <option value="cancel">ยกเลิกการสั่งซื้อ</option>
                                        <option value="success">ทำรายการเสร็จสิ้น</option>
                                    </select>
                                    <button type="submit" name="ord_id" value="<?= $item['ord_id'] ?>">ตกลง</button>
                                    <button type="button" onclick="update_order_status(this, false)">ยกเลิก</button>
                                </div>
                            </form>
                        </td>
                        <td><?= $item['ord_date'] ?></td>
                        <td>
                            <?php if ($item['ord_status'] == 'success' || !empty($item['pay_amount'])) : ?>
                                <button class="btn btn-default" onclick="onPrintInvoice(<?= $item['ord_id'] ?>)">
                                    <i class="glyphicon glyphicon-print"></i> พิมพ์ใบเสร็จ
                                </button>      
                            <?php else : ?>
                                <button class="btn btn-default disabled" disabled="">
                                    <i class="glyphicon glyphicon-hourglass"></i> รอดำเนินการ
                                </button>  
                            <?php endif; ?>                          
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="5">รายการสั่งซื้อทั้งหมด <?= count($orders) ?> รายการ</td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>