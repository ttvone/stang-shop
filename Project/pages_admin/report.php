<div class="form-group">
    <div class="pull-right hidden-print">
        <a href="<?= get_url('/admin.php') ?>" class="btn btn-default">กลับไปหน้าหลัก</a>
    </div>
    <h3>ข้อมูลรายงานยอดขายสินค้า</h3>
</div>
<br class="hidden-print">
<div class="row">
    <div class="col-sm-4 hidden-print">
        <form method="post">
            <div class="form-group">
                <label for="date_from">จากวันที่ :</label>
                <input type="date" class="input" id="date_from" name="date_from" value="<?= set_value('date_from') ?>" />
            </div>

            <div class="form-group">
                <label for="date_to">ถึงวันที่ :</label>
                <input type="date" class="input" id="date_to" name="date_to" value="<?= set_value('date_to') ?>" />
            </div>
            <!-- <div class="form-group">
                <label for="date">วันที่สั่งซื้อ :</label>
                <select id="date" name="date">
                    <option value="">ไม่เลือกวัน</option>
                    <?php for ($i = 1; $i <= 31; $i++) : ?>
                        <option value="<?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>" <?= set_value('date') == $i ? 'selected' : '' ?>>
                            <?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>
                        </option>
                    <?php endfor; ?>
                </select>
            </div>

            <div class="form-group">
                <label for="month">เดือนที่สั่งซื้อ :</label>
                <select id="month" name="month">
                    <option value="">ไม่เลือกเดือน</option>
                    <?php for ($i = 1; $i <= 12; $i++) : ?>
                        <option value="<?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>" <?= set_value('month') == $i ? 'selected' : '' ?>>
                            <?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>
                        </option>
                    <?php endfor; ?>
                </select>
            </div>

            <div class="form-group">
                <label for="year">ปีที่สั่งซื้อ :</label>
                <select id="year" name="year">
                    <option value="">ไม่เลือกปี</option>
                    <?php $years = get_min_date_order() ?>
                    <?php for ($i = $years['min']; $i <= $years['max']; $i++) : ?>
                        <option <?= set_value('year') == $i ? 'selected' : '' ?>>
                            <?= $i ?>
                        </option>
                    <?php endfor; ?>
                </select>
            </div> -->

            <div class="form-group">
                <button type="submit" class="btn btn-default button btn-block">
                    <i class="glyphicon glyphicon-search"></i> ดูยอดขาย
                </button>

                <button type="button" class="btn btn-danger button btn-block" onclick="print()">
                    <i class="glyphicon glyphicon-print"></i> พิมพ์รายงาน
                </button>
            </div>
        </form>
    </div>
    <div class="col-sm-8">
        <p class="form-group">
            <u>
            รายงายยอดขายของ
            <?php if(!empty($date_from_search = get_input('date_from')) && !empty($date_to_search = get_input('date_to'))) : ?>
                วันที่ <?= $date_from_search ?> ถึงวันที่ <?= $date_to_search ?>
            <?php else : ?>
                ทุกวัน
            <?php endif; ?>
            </u>
        </p>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>เลขที่สั่งซื้อ</th>
                        <th>วันที่สั่งซื้อ</th>
                        <th>ยอดที่ขายได้</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $price_all = 0; ?>
                    <?php foreach (get_orders() as $item) : ?>
                        <tr>
                            <td>
                                <a href="<?= get_url('/cart.php?site=order-invoice&id=' . $item['ord_number']) ?>" class="hidden-print">
                                    <u><?= $item['ord_number'] ?></u>
                                </a>
                                <span class="visible-print-inline">
                                    <?= $item['ord_number'] ?>
                                </span>
                            </td>
                            <td><?= $item['ord_date'] ?></td>
                            <td>
                                <?php
                                $price = $item['price_all'] + $item['ord_tax'] + $item['ship_price'];
                                $price_all += $price;
                                ?>
                                <?= number_format($price, 2) ?> บาท
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th class="text-right" colspan="2">รวมยอดขาย</th>
                        <th><?= number_format($price_all, 2) ?> บาท</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<br>
