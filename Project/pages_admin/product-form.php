<?php
$product = get_product_by_id(set_value('prod_id', 0));
$action = empty($product) ? 'create' : 'update';
?>
<h3>เพิ่มข้อมูลสินค้าใหม่</h3>
<br>
<form method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="prod_name">ชื่อสินค้า : <b class="required">*</b></label>
                <input type="text" name="prod_name" id="prod_name" required="" value="<?= set_value('prod_name', $product['prod_name']) ?>">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="cate_id">ประเภทสินค้า : <b class="required">*</b></label>
                <select type="text" name="cate_id" id="cate_id" required="">
                    <option value="">เลือกประเภทสินค้า</option>
                    <?php foreach (get_categories() as $item) : ?>

                        <option value="<?= $item['cate_id'] ?>" <?= set_value('cate_id', $product['cate_id']) == $item['cate_id'] ? 'selected' : '' ?>>
                            <?= $item['cate_name'] ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="prod_price">ราคาสินค้า : <b class="required">*</b></label>
                <input type="number" name="prod_price" id="prod_price" required="" value="<?= set_value('prod_price', $product['prod_price']) ?>">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="prod_amount">จำนวนสินค้า : <b class="required">*</b></label>
                <input type="number" name="prod_amount" id="prod_amount" required="" value="<?= set_value('prod_amount', $product['prod_amount']) ?>">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="prod_detail">รายละเอียดสินค้า :</label>
                <textarea name="prod_detail" id="prod_detail" rows="5"><?= set_value('prod_detail', $product['prod_detail']) ?></textarea>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="prod_images">รูปภาพสินค้า :</label>
                <input type="file" name="prod_images[]" id="prod_images" multiple accept="image/*">
                <?php if (!empty($product)) : ?>
                    <!--รหัสสินค้า-->
                    <input type="hidden" name="prod_id" value="<?= $product['prod_id'] ?>">
                    <!--ภาพสินค้า-->
                    <div class="image-example">
                        <?php foreach (get_product_images($product['prod_id']) as $img) : ?>
                            <div class="image-container" id="image-<?= $img['image_id'] ?>">
                                <!--เครื่องหมายลบรูปภาพ-->
                                <a href="<?= get_site('product', ['product_image_delete' => $img['image_id']]) ?>" 
                                   class="glyphicon glyphicon-remove" delete-id="image-<?= $img['image_id'] ?>" ajax-delete-element="คุณต้องการลบภาพนี้จริงหรือ"></a>
                                <a href="/images?id=<?= $img['image_id'] ?>" target="_blank">
                                    <img src="/images?id=<?= $img['image_id'] ?>" class="img-thumbnail" />
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <button class="btn btn-info button" type="submit" name="prod_button" value="<?= $action ?>">
            <?= (empty($product) ? 'บันทึกข้อมูล' : 'แก้ไขข้อมูล') ?>
        </button>
        <span class="hidden-xs">|</span>
        <a href="<?= $_SERVER['HTTP_REFERER'] ?>" class="btn btn-default button">
            กลับไปยังหน้ารายการ
        </a>
    </div>
</form>
