<div class="form-group">
    <div class="pull-right hidden-print">
        <a href="<?= get_url('/admin.php') ?>" class="btn btn-default">กลับไปหน้าหลัก</a>
        <a href="" onclick="print(); return false;" class="btn btn-danger">
            <i class="glyphicon glyphicon-print"></i> พิมพ์รายงาน
        </a>
    </div>
    <h3>ข้อมูลรายงานสินค้าคงเหลือ</h3>
</div>
<br>
<div class="row stoct-report">
    <div class="col-sm-12"> 
        <div class="form-group well">
            <h4>สินค้าคงเหลือทั้งหมด</h4>
            <table class="table">
                <thead>
                    <tr>
                        <th>สินค้า</th>
                        <th>จำนวนที่เหลือ</th>
                        <th>รายละเอียด</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $total = 0 ?>
                    <?php foreach (get_products() as $item) : ?>
                        <?php $total += $item['prod_amount'] ?>
                        <tr>
                            <td><?= $item['prod_name'] ?></td>
                            <td><?= $item['prod_amount'] ?> ชื้น</td>
                            <td>
                                <a href="<?= get_url('/single.php', ['prod_id' => $item['prod_id']]) ?>">
                                    <u>ดูรายละเอียด</u>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>รวมทั้งสิ้น</th>
                        <th><?= $total ?> ชื้น</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="col-sm-6">

        <div class="form-group well">
            <h4>สินค้าที่ขายขายหมดแล้ว</h4>
            <table class="table">
                <thead>
                    <tr>
                        <th>สินค้า</th>
                        <th>ขายออกไป</th>
                        <th>รายละเอียด</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $total = 0 ?>
                    <?php foreach (get_select_product_losed() as $item) : ?>
                        <?php $total += $item['amount'] ?>
                        <tr>
                            <td><?= $item['prod_name'] ?></td>
                            <td><?= $item['amount'] ?> ชื้น</td>
                            <td>
                                <a href="<?= get_url('/single.php', ['prod_id' => $item['prod_id']]) ?>">
                                    <u>ดูรายละเอียด</u>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>รวมทั้งสิ้น</th>
                        <th><?= $total ?> ชื้น</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group well">
            <h4>สินค้าขายไปแล้ว</h4>
            <table class="table">
                <thead>
                    <tr>
                        <th>สินค้า</th>
                        <th>ขายไปแล้ว</th>
                        <th>รายละเอียด</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $total = 0 ?>
                    <?php foreach (get_select_product_sale() as $item) : ?>
                        <?php $total += $item['amount'] ?>
                        <tr>
                            <td><?= $item['prod_name'] ?></td>
                            <td><?= $item['amount'] ?> ชื้น</td>
                            <td>
                                <a href="<?= get_url('/single.php', ['prod_id' => $item['prod_id']]) ?>">
                                    <u>ดูรายละเอียด</u>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>รวมทั้งสิ้น</th>
                        <th><?= $total ?> ชื้น</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>

    </div>
</div>