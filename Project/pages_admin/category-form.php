<?php
$category = get_category_by_id(set_value('cate_id'));
$save_type = $category ? 'update' : 'create';
?>
<form method="post">
    <div class="row">
        <div class="col-sm-7">
            <?php if (empty($category)) : ?>
                <h3>เพิ่มข้อมูลประเภทสินค้าใหม่</h3>
            <?php else : ?>
                <input type="hidden" name="cate_id" value="<?= $category['cate_id'] ?>">
                <h3>แก้ไขข้อมูลประเภทสินค้า</h3>
            <?php endif; ?>
            <br>
            <div class="form-group">
                <label for="cate_name">ชื่อประเภทสินค้า : </label>
                <input type="text" name="cate_name" id="cate_name" required="" value="<?= set_value('cate_name', $category['cate_name']) ?>">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-info button" name="cate_save" value="<?= $save_type ?>">
                    บันทึกข้อมูล
                </button>
                <a href="<?= get_back() ?>" class="btn btn-default button">
                    กลับไปหน้ารายการ
                </a>
            </div>
        </div>
    </div>
</form>
