<?php

switch (get_input('page')) {
    case 'form':
        load_admin('product-form');
        break;
    case 'detail':
        load_admin('product-detail');
        break;
    default :
        load_admin('product-item');
        break;
}