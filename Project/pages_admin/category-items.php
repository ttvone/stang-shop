<h3>จัดการข้อมูลประเภทสินค้า</h3>
<br>
<div class="form-group">
    <div class="pull-right">
        <a href="<?= get_site('category', ['page' => 'form']) ?>" class="btn btn-info">เพิ่มข้อมูลใหม่</a>
        <a href="<?= get_url('/admin.php') ?>" class="btn btn-default">กลับไปหน้าหลัก</a>
    </div>
    <h4>
        ตาราง แสดงรายการข้อมูลประเภทสินค้า 
    </h4>
</div>
<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th>รหัสประเภท</th>
                <th>ชื่อประเภท</th>
                <th>วันที่</th>
                <th>จัดการ</th>
            </tr>
        </thead>
        <tbody>
            <?php $categories = get_categories() ?>
            <?php foreach ($categories['items'] as $item) : ?>
                <tr>
                    <td><?= $item['cate_id'] ?></td>
                    <td><?= $item['cate_name'] ?></td>
                    <td><?= $item['cate_created'] ?></td>
                    <td>
                        <form class="inline-form" method="post" action="<?= get_site('category', ['page' => 'form']) ?>">
                            <button type="submit" name="cate_id" value="<?= $item['cate_id'] ?>" class="btn btn-info btn-xs">แก้ไข</button>
                        </form>
                        <a href="<?= get_site('category', ['cate_delete' => $item['cate_id']]) ?>" class="btn btn-danger btn-xs" ajax-delete="คุณต้องการลบข้อมูลจริงหรือ">
                            ลบทั้ง
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">รายการข้อมูลทั้งหมด <?= $categories['rows'] ?> รายการ</td>
            </tr>
        </tfoot>
    </table>
</div>

<div class="text-center">
    <?= pagination_template('category', $categories) ?>
</div>
