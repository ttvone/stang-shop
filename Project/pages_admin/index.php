<div class="">
    <h3>จัดการข้อมูลหลังบ้าน Administrator</h3>
    <br>
    <div class="row">
        <div class="col-sm-4">
            <a class="admin-boxs" href="<?= get_site('category') ?>">
                <div class="form-group">
                    <img src="<?= get_url('/images/category-icon.png') ?>" />
                </div>
                <h4>ข้อมูลประเภทสินค้า</h4>
            </a>
        </div>

        <div class="col-sm-4">
            <a class="admin-boxs" href="<?= get_site('product') ?>">
                <div class="form-group">
                    <img src="<?= get_url('/images/product-icon.png') ?>" />
                </div>
                <h4>ข้อมูลสินค้า</h4>
            </a>
        </div>

        <div class="col-sm-4">
            <a class="admin-boxs" href="<?= get_site('order') ?>">
                <div class="form-group">
                    <img src="<?= get_url('/images/order-icon.png') ?>" />
                </div>
                <h4>ข้อมูลสั่งซื้อจากลูกค้า</h4>
            </a>
        </div>

        <div class="col-sm-4">
            <a class="admin-boxs" href="<?= get_site('payment') ?>">
                <div class="form-group">
                    <img src="<?= get_url('/images/payment-icon.png') ?>" />
                </div>
                <h4>ข้อมูลแจ้งการชำระเงิน</h4>
            </a>
        </div>

        <div class="col-sm-4">
            <a class="admin-boxs" href="<?= get_site('report') ?>">
                <div class="form-group">
                    <img src="<?= get_url('/images/report-icon.png') ?>" />
                </div>
                <h4>รายงานยอดขายสินค้า</h4>
            </a>
        </div>

        <div class="col-sm-4">
            <a class="admin-boxs" href="<?= get_site('stock') ?>">
                <div class="form-group">
                    <img src="<?= get_url('/images/shipping-icon.png') ?>" />
                </div>
                <h4>รายงานสินค้าคงเหลือ</h4>
            </a>
        </div>
    </div>
</div>
