<h3>จัดการข้อมูลประเภทสินค้า</h3>
<br>
<div class="form-group">
    <div class="pull-right">
        <a href="<?= get_site('product', ['page' => 'form']) ?>" class="btn btn-info">เพิ่มข้อมูลใหม่</a>
        <a href="<?= get_url('/admin.php') ?>" class="btn btn-default">กลับไปหน้าหลัก</a>
    </div>
    <h4>
        ตาราง แสดงรายการข้อมูลประเภทสินค้า 
    </h4>
    <div class="clearfix"></div>
</div>
<div class="table-responsive">
    <table class="table table-hover table-midle">
        <thead>
            <tr>
                <th class="text-center">สินค้า</th>
                <th>ชื่อสินค้า</th>
                <th>ประเภทสินค้า</th>
                <th>ราคา/จำนวน</th>
                <th>จัดการ</th>
            </tr>
        </thead>
        <tbody>
            <?php $products = get_products(); ?>
            <?php foreach ($products['items'] as $item): ?>
                <?php $detail_link = get_site('product', ['page' => 'detail', 'product_detail' => $item['prod_id']]) ?>
                <tr>
                    <td class = "image-td text-center">
                        <a href="<?= $detail_link ?>">
                            <img src = "<?= get_url("/images?prod_id={$item['prod_id']}") ?>" class = "img-thumbnail"/>
                        </a>
                    </td>
                    <td>
                        <a href="<?= $detail_link ?>">
                            <u><?= $item['prod_name'] ?></u>
                        </a>
                    </td>
                    <td><?= $item['cate_name'] ?></td>
                    <td>
                        <?= $item['prod_price'] ?> บาท
                        /
                        <?= $item['prod_amount'] ?> ชื้น
                    </td>
                    <td>
                        <form action="<?= get_site('product', ['page' => 'form']) ?>" method="post" class="inline-form">
                            <button href="" class="btn btn-info btn-xs" name="prod_id" value="<?= $item['prod_id'] ?>">แก้ไข</button>
                        </form>
                        <a href="<?= get_site('product', ['product_delete' => $item['prod_id']]) ?>" ajax-delete="คุณต้องการลบสินค้านี้จริงหรือ?" class="btn btn-danger btn-xs">ลบทิ้ง</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="5">
                    รายการข้อมูลทั้งหมด <?= $products['rows'] ?> รายการ
                </td>
            </tr>
        </tfoot>
    </table>
</div>

<div class="text-center">
    <?= pagination_template('product', $products) ?>
</div>