<?php
$payments = get_payments();
?>
<h3>ข้อมูลแจ้งการชำระเงินจากลูกค้า</h3>
<br>
<div class="form-group">
    <div class="pull-right">
        <a href="<?= get_url('/admin.php') ?>" class="btn btn-default">กลับไปหน้าหลัก</a>
    </div>
    <h4>
        ตาราง แสดงรายการข้อมูลแจ้งการชำระเงินจากลูกค้า
    </h4>
</div>
<br>
<div class="form-group">
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>เลขที่สั่งซื้อ</th>
                    <th>วันที่ชำระเงิน</th>
                    <th>เวลาชำระเงิน</th>
                    <th>จำนวนเงินที่ชำระ</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($payments['items'] as $item) : ?>
                <?php $order_number = str_pad($item['ord_id'], 5, '0', STR_PAD_LEFT); ?>
                    <tr>
                        <td>
                            <a href="<?= get_url('/cart.php', ['site' => 'order-invoice', 'id' => $order_number ]) ?>">
                                <u><?= $order_number ?></u>
                            </a>
                        </td>
                        <td><?= $item['pay_date'] ?></td>
                        <td><?= $item['pay_time'] ?></td>
                        <td><?= $item['pay_amount'] ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="5">รายการแจ้งชำระเงินทั้งหมด <?= count($payments) ?> รายการ</td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<div class="text-center">
    <?= pagination_template('product', $payments) ?>
</div>