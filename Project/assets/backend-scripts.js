$(function () {
    // สำหรับ confirm เวลาคลิ๊ก
    $('[confirm]').on('click', function () {
        var confirm_message = $(this).attr('confirm');
        if (!confirm(confirm_message)) {
            return false;
        }
    });

    // สำหรับ ลบข้อมูลแล้ว refresh หน้า ภ้าสำเร็จ
    $('a[ajax-delete]').on('click', function () {
        var confirm_message = $(this).attr('ajax-delete');
        if ($.trim(confirm_message) !== '') {
            if (!confirm(confirm_message)) {
                return false;
            }
        }
        $.ajax({
            url: this.href,
            method: 'get',
            success: function () {
                location.reload();
            },
            error: function (res) {
                alert(res.statusText || 'เกิดข้อผิดพลาดไม่สามารถทำรายการได้');
            }
        });
        return false;
    });

    // สำหรับลบข้อมูลแล้ว ลบ element นั้นๆ
    $('a[ajax-delete-element]').on('click', function () {
        if (confirm($(this).attr('ajax-delete-element'))) {
            var deleteId = $(document.getElementById($(this).attr('delete-id')));
            $.ajax({
                url: this.href,
                method: 'get',
                success: function () {
                    deleteId.remove();
                },
                error: function (res) {
                    alert(res.statusText || 'เกิดข้อผิดพลาดไม่สามารถทำรายการได้');
                }
            });
        }
        return false;
    });
});

// เปลี่ยนสถานะของการสั่งซื้อ การส่งสินค้า และการชำระเงินของการสั่งซื้อ
function update_order_status(element, is_open) {
    var form = $(element).parent('.box-status').parent('form');
    var show_status = form.find('.show-status');
    var update_status = form.find('.update-status');
    show_status.hide();
    update_status.hide();
    if (is_open) {
        update_status.show();
        update_status.find('.select_status').val($(element).attr('data-value'));
    } else {
        show_status.show();
    }
    return false;
}

// พิมพ์ใบเสร็จชำระเงิน
function onPrintInvoice(ord_id) {
    var request = $.get('/invoice.php?ord_id=' + ord_id);
    request.done(function(response) {
        var width = 650;
        var newWindow = window.open('', 'PRINT', 'height=' + width + ', width=' + width + ', left=' + (screen.width / 2) - (650 / 2));
        newWindow.document.write(response);
        newWindow.document.close();
        newWindow.focus();
        newWindow.print();
        newWindow.close();
    });
}