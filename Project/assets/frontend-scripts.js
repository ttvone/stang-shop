// init page
$(function () {
    // for nav menu hover
    $(".megamenu").megamenu();
    // for slider image
    $('.flexslider').flexslider({
        animation: "slide",
        start: function (slider) {
            $('body').removeClass('loading');
        }
    });


    // for single page
    $('#etalage').etalage({
        thumb_image_width: 300,
        thumb_image_height: 400,
        source_image_width: 900,
        source_image_height: 1200,
        show_hint: true,
        click_callback: function (image_anchor, instance_id) {
            alert('Callback example:\nYou clicked on an image with the anchor: "' + image_anchor + '"\n(in Etalage instance: "' + instance_id + '")');
        }
    });
});

// ฟังชั่นก์แก้ไข จำนวนที่สั่งในตะกร้าสินค้า
function cart_focus_product_amount(input, isFocus) {
    var inputElement = $(input);
    var button_btn = '<span class="input-group-btn"><button class="btn btn-default" type="submit">แก้ไข</button></span>';
    var button_txt = '<span class="input-group-addon">ชิ้น</span>';
    var data_value = inputElement.attr('data-value');
    var data_amount = inputElement.attr('data-amount');
    var value = inputElement.val();
    // ถ้า input มีการ focus
    if (isFocus) {
        inputElement.next('span').remove();
        inputElement.after(button_btn);
    }
    // ถ้า input ยกเลิก focus
    // ถ้าข้อมูลเก่าเท่ากับข้อมูลใหม่
    else if (data_value == value) {
        inputElement.next('span').remove();
        inputElement.after(button_txt);
    }
    // ถ้ากรอกมาใหม่ใช่ตัวเลข
    else if (!$.isNumeric(value)) {
        inputElement.val(data_value);
    }
    // ถ้าข้อมูลที่กรอกมากกว่าที่มีอยู่
    else if (data_amount < parseFloat(value)) {
        alert('สินค้าของเรามีแค่ ' + data_amount + ' ชื้นเท่านั้น');
        inputElement.val(data_value);
    }
    // ถ้าข้อมูลเก่าใหม่ไม่เท่ากัน
    else {
        alert('กรุณากดคำว่าแก้ไขเพื่อแก้ไขสินค้า');
    }
}

function go_to_orders(url) {
    var order_number = prompt('กรอกหมายเลขสั่งซื้อสินค้า 10 หลัก ของคุณ');
    if (order_number) {
        location.href = url + '&id=' + order_number;
    }
    return false;
}
