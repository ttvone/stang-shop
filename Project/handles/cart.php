<?php

// เก็บสินค้าไว้ใน ตะกร้าสินค้า
if (!empty(get_input('product_into_cart'))) {
    $product_id = get_input('product_into_cart');
    if (get_product_by_id($product_id) == 0) {
        alert_error(404, 'ไม่มีหน้านี้ในระบบ');
        die;
    }

    // เพิ่มสินค้าเข้า cart_store
    cart_store($product_id);
    redirect(get_url('/cart.php'), false);
}
// แก้ไขสินค้าในตระกร้าสินค้า
if (!empty(get_input('product_edit_cart') && !empty(get_input('amount')))) {
    $product_id = get_input('product_edit_cart');
    $amount = get_input('amount');

    // แก้ไขสินค้าจาก cart_store
    cart_store($product_id, $amount, true);
    redirect(get_url('/cart.php'), false);
}
// ลบสินค้า ออกแค่ 1 รายการจาก product_id
if (!empty(get_input('product_delete_cart'))) {
    $product_id = get_input('product_delete_cart');

    // ลบสินค้าจาก cart_store
    cart_store($product_id, -1);
    redirect(get_url('/cart.php'), false);
}
// ลบสินค้า ออกจาก ตระกร้าสินค้าทั้งหมด
if (!empty(get_input('product_clear_cart'))) {
    cart_store(-1);
    redirect(get_url('/cart.php'), false);
}


// บันทึกข้อมูลการสั่งซื้อสินค้า เข้าสู่ระบบ
if (get_input('shipping_button') === 'send') {
    $ship_name = get_input('ship_name');
    $ship_email = get_input('ship_email');
    $ship_phone = get_input('ship_phone');
    $ship_address = get_input('ship_address');

    $cart = get_cart_amount(true);

    if (!empty($ship_name) && !empty($ship_email) && !empty($ship_phone) && !empty($ship_address) && !empty($cart)) {
        $ord_number = time();
        $ord_tax = $cart['tax'];
        // ค่าขนส่งสินค้า
        $ship_price = $cart['shipping'];
        // สำหรับบันทึกข้อมูล การสั่งซื้อและการจัดส่ง เข้าตาราง orders
        $order_sql = <<<eot
            insert into orders set
                ord_number      = '$ord_number',
                ord_tax         = '$ord_tax',
                ship_name       = '$ship_name',
                ship_email      = '$ship_email',
                ship_phone      = '$ship_phone',
                ship_address    = '$ship_address',
                ship_price      = '$ship_price'
eot;
        // บันทึกเข้าตาราง orders
        if (database_query($order_sql) > 0) {
            // สำกรับบันทึกเข้าตาราง orders_detail
            $order_detail_sel = "";
            $ord_id = database_insert_id();
            foreach ($cart['products'] as $product) {
                $prod_id = $product['prod_id'];
                $detail_amount = $product['cart_amount'];
                $detail_price = $product['prod_price'];
                // เช็คว่าสินค้ามีพอหรือไม่
                if ($detail_amount <= $product['prod_amount']) {
                    $order_detail_sel = <<<eot
                    insert into orders_detail set
                        ord_id          = '$ord_id',
                        prod_id         = '$prod_id',
                        detail_amount   = '$detail_amount',
                        detail_price    = '$detail_price';
                    \n
eot;
                    // บันทึกรายระเอียดการสั่งซื้อเข้าตาราง order detail
                    if (database_query($order_detail_sel) > 0) {
                        $minum_amoung = $product['prod_amount'] - $detail_amount;
                        // update จำนวนสินค้า
                        database_query("update product set prod_amount = $minum_amoung where prod_id = $prod_id");
                        // update $ord_number ใหม่
                        $ord_number = str_pad($ord_id, 5, '0', STR_PAD_LEFT);
                        database_query("update orders set ord_number = '$ord_number' where ord_id = $ord_id");
                    }
                }
                // กรณีสินค้าหมด
                else {
                    alert('สินค้า "' . $product['prod_name'] . '" มีไม่พอ ทางเราขออนุญาติตัดสินค้านี้ออกจากรายการสั่งซื้อของคุณ');
                }
            }
            // เคลียร์ตะกร้าสินค้า
            cart_store(-1);
            redirect(get_site('order-invoice', ['id' => $ord_number]), false);
        }
    }
    alert_error(500, 'เกิดข้อผิดพลาดไม่สามารถทำรายการได้');
}

// select ข้อมูลสินค้า
function get_product_by_id($prod_id) {
    return database_num_rows("select * from product where prod_id=$prod_id");
}

// select ข้อมูลสินค้าที่เชื่อมโยงกับ cart_store
function get_product_by_store() {
    $store = select_object_to_single_array(cart_store(), 'prod_id');
    $prod_ids = join(',', $store) ?: 0;
    $sql = <<< eot
            select * from product inner join category
            on product.cate_id = category.cate_id
            where prod_id in ($prod_ids)
eot;
    return database_select_all($sql) ?: [];
}

// select ข้อมูลการสั่งซ์้อสินค้า
function get_orders_by_ord_number($ord_number) {
    $order = database_select_row("select * from orders where ord_number = '$ord_number'");
    if (!empty($order)) {
        $ord_id = $order['ord_id'];
        $detail = database_select_all("select * from orders_detail inner join product on orders_detail.prod_id = product.prod_id where ord_id = $ord_id");
        return [
            'order' => $order,
            'detail' => $detail
        ];
    }
    return null;
}
