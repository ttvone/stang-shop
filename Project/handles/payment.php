<?php
// บันทึกข้อมูลแจ้งชำระเงิน
if(!empty(get_input('pay_submit'))){
    $ord_id = (get_input('ord_id') * 1);
    $pay_date = get_input('pay_date');
    $pay_time = get_input('pay_time');
    $pay_amount = get_input('pay_amount');

    $query = "replace into payment set  ord_id = $ord_id, pay_date = '$pay_date', pay_time = '$pay_time', pay_amount = $pay_amount";

    if(database_query($query) > 0){
        alert('แจ้งการชำระเงินเรียบร้อยแล้ว', 200);
        redirect(get_url('/'), true);
        exit();
    }
    alert_error('ไม่สามารถลบข้อมูลได้', database_error());
}


// ดึงข้อมูลจากตารางแจ้งชำระเงิน payment
function get_payments(){
    $query = 'select * from payment';
    return get_pagination($query, PAGE_MAX);
}