<?php

// select ข้อมูลสินค้าออกมาชุดเดียว
function get_product($prod_id) {
    $sql = <<< eot
            select * from
                product inner join category
                on product.cate_id = category.cate_id
                where prod_id=$prod_id
eot;
    return database_select_row($sql);
}

// select ข้อมูลรูปภาพสินค้า จาก prod_id
function get_product_images($prod_id) {
    return database_select_all("select * from product_image where prod_id=$prod_id");
}

// บันทึกข้อมูลการรับชมสินค้า อาจจะ query ข้อมูล ซํบซ้อนนิดหน่อยแต่พยายามให้ง่ายที่สุดแล้วนะครับ
function insert_view_count_product($prod_id) {
    $sql = <<< eot
          update product
              join (select prod_view_count from product where prod_id = $prod_id) counter
              set product.prod_view_count = (counter.prod_view_count + 1)
              where product.prod_id = $prod_id;
eot;
    database_query($sql);
}
