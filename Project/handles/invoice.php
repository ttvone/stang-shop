<?php
if(empty($ord_id = get_input('ord_id'))) {
    return redirect('/');
}

function get_invoice() {
    $ord_id = get_input('ord_id');
    $query = "
    select 
        orders.*,
        payment.*,
        orders.ord_id
    from 
        orders left join payment 
        on orders.ord_id = payment.ord_id 
    where orders.ord_id = $ord_id
    order by 
        orders.ord_date desc";
    $order = database_select_row($query);
    if (!empty($order)) {
        $detail_query = "
            select 
                *
            from 
                orders_detail 
                inner join 
                product
            on 
                product.prod_id = orders_detail.prod_id 
                inner join 
                category
            on
                product.cate_id = category.cate_id
            where orders_detail.ord_id = $ord_id";
        $order['details'] = database_select_all($detail_query);
    }
    else {
        redirect('/');
        exit;
    }
    return $order;
}