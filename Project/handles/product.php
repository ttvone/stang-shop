<?php

// เพิ่มข้อมุลสินค้า
if (get_input('prod_button') === 'create') {
    $prod_name = get_input('prod_name');
    $prod_price = get_input('prod_price');
    $prod_amount = get_input('prod_amount');
    $prod_detail = get_input('prod_detail');
    $cate_id = get_input('cate_id');
    $product_query = "
        insert into product set
            prod_name   = '$prod_name',
            prod_price  = $prod_price,
            prod_amount = $prod_amount,
            prod_detail = '$prod_detail',
            cate_id     = $cate_id";
    if (database_query($product_query) > 0) {
        // อัพโหลดรูปภาพ
        update_product_image(database_insert_id());
        // กลับไปหน้า product
        redirect(get_site('product'), true);
    }

    alert_error(500, 'บันทึกข้อมูลไม่สำเร็จ : <br>' . database_error());
}

// ลบข้อมูลสินค้า
if (!empty(get_input('product_delete'))) {
    if (database_query('delete from product where prod_id=' . get_input('product_delete'))) {
        redirect(get_site('product'));
    }
    alert_error('ไม่สามารถลบข้อมูลได้', database_error());
}

// แก้ไขสินค้า
if (get_input('prod_button') === 'update') {
    $prod_id = get_input('prod_id');
    $prod_name = get_input('prod_name');
    $prod_price = get_input('prod_price');
    $prod_amount = get_input('prod_amount');
    $prod_detail = get_input('prod_detail');
    $cate_id = get_input('cate_id');
    $product_query = "
        update product set
            prod_name       = '$prod_name',
            prod_price      = $prod_price,
            prod_amount     = $prod_amount,
            prod_detail     = '$prod_detail',
            prod_created    = now(),
            cate_id         = $cate_id
            where prod_id   = $prod_id
        ";
    if (database_query($product_query) > 0) {
        // อัพโหลดรูปภาพ
        update_product_image($prod_id);
        // กลับไปหน้า product
        redirect(get_site('product'), true);
    }

    alert_error(500, 'บันทึกข้อมูลไม่สำเร็จ : <br>' . database_error());
}

// ลบภาพสินค้า
if (!empty(get_input('product_image_delete'))) {
    $image_id = get_input('product_image_delete');
    if (database_query("delete from product_image where image_id=$image_id")) {
        redirect(get_site('product'));
    }
    alert_error(500, 'เกิดข้อผิดพลาดลบรูปภาพไม่ได้');
}

// สำหรับอัพโหลดรูปภาพสินค้า
function update_product_image($prod_id) {
    // upload ภาพ
    $prod_images = $_FILES['prod_images'];
    if (!empty($prod_images['name'][0])) {
        $count = count($prod_images['name']);
        for ($index = 0; $index < $count; $index++) {
            $image_name = $prod_images['name'][$index];
            $image_content = $prod_images['tmp_name'][$index];
            $image_size = $prod_images['size'][$index];
            $image_type = $prod_images['type'][$index];
            // ตรวจสอบว่า Error หรือไม่
            $image_error = $prod_images['error'][$index];
            if ($image_error === 0) {
                $image_content = database_escape_string(file_get_contents($image_content));
                $product_image_query = "insert into product_image set
                        image_name      = '$image_name',
                        image_content   = '$image_content',
                        image_size      = $image_size,
                        image_type      = '$image_type',
                        prod_id         = $prod_id";
                database_query($product_image_query);
            }
            // if error
            else {
                alert("เกิดข้อผิดพลาดกับรูป \"$image_name\" : " . file_upload_errors($image_error));
            }
        }
    }
}

// select ข้อมูลสินค้า และ ประเภทสินคา หลายข้อมูล
function get_products() {
    return get_pagination('select * from product inner join category on product.cate_id = category.cate_id order by prod_created desc', PAGE_MAX);
}

// select ข้อมูลสินค้า และ ประเภทสินคา ข้อมูลเดียว
function get_product_by_id($prod_id) {
    return database_select_row("select * from product inner join category on product.cate_id = category.cate_id where prod_id=$prod_id");
}

// select ข้อมูลประเภทสินค้า
function get_categories() {
    return database_select_all('select * from category order by cate_id desc');
}

// select ข้อมูลรูปภาพสินค้า
function get_product_images($prod_id) {
    return database_select_all("select * from product_image where prod_id=$prod_id");
}
