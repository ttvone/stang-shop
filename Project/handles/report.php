<?php

// เรียกดูข้อมูลการสั่งซื้อจากตาราง orders
function get_orders() {
    $date_from = get_input('date_from');
    $date_to = get_input('date_to');
    $where_and = '';
    // เช็ค where beetween
    if($date_from && $date_to) {
        $where_and = "and DATE(DATE_FORMAT(ord_date,'%y-%m-%d')) between '$date_from' and '$date_to'";
    }
    $query = <<< eot
            select
                ord_id,
                ord_number,
                ord_date,
                ord_tax,
                ship_price,
                ship_status,
                pay_status,
                ord_status,
                ((select sum(detail_amount * detail_price) from orders_detail where orders.ord_id = orders_detail.ord_id)) as price_all
            from orders 
            where 
            (pay_status = 'paid' or ord_status = 'success')
            $where_and
            order by ord_date desc
eot;
    return database_select_all($query);
}

// เรียกดูข้อมูลเดือนที่น้อยที่สุด
function get_min_date_order() {
    return [
        'min' => database_select_row('select DATE_FORMAT(min(ord_date), \'%Y\') as min from orders')['min'] ?: 0,
        'max' => database_select_row('select DATE_FORMAT(max(ord_date), \'%Y\') as max from orders')['max'] ?: 0
    ];
}
