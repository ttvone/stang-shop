<?php

// คำนวนหาค่าเลือกดูรายการสินค้าจากราคาสินค้า
function get_category_prices($split_price) {
    $max_price = database_select_row('select max(prod_price) as max from product')['max'];
    $category_prices = [];
    if (!empty($max_price) && $max_price >= $split_price) {
        $max_price = ceil($max_price / $split_price);
        for ($index = 0; $index < $split_price; $index++) {
            $category_prices[] = [
                ($max_price * $index),
                ($max_price * ($index + 1))
            ];
        }
    }
    return $category_prices;
}

// แสดงค่าประเภทสินค้าออกมา
function get_categories() {
    return database_select_all('select * from category order by cate_created desc');
}

// แสดงรายการสินค้ามาใหม่
function get_new_products() {
    return database_select_all('select * from product order by prod_id desc limit 0, 5');
}

// แสดงรายการสินค้าอัพเดตล่าสุด
function get_last_products() {
    return database_select_all('select * from product order by prod_created desc limit 0, 5');
}

// แสดงรายการสินค้าเพื่อนจะใส่ไปรายการหลัก
function get_product_dashboard() {
    $sql = 'select * from
                product inner join category
                on product.cate_id = category.cate_id ';
    // ค้นหาข้อมูลจากชื่อสินค้า
    if (!empty($search = get_input('search'))) {
        $sql .= "where prod_name like '%$search%' ";
    }
    // ค้นหาข้อมูลจาก ราคาระหว่างช่วงสินค้า
    elseif (is_numeric($price_from = get_input('price_form')) && is_numeric($price_to = get_input('price_to'))) {
        $sql .= "where prod_price between $price_from and $price_to ";
    }
    // ค้นหาข้อมูลจาก หมวดหมู่สินค้า
    elseif (!empty($cate_id = get_input('cate_id'))) {
        $sql .= "where product.cate_id = $cate_id ";
    }
    $sql .= 'order by prod_id desc';
    return get_pagination($sql, 12);
}
