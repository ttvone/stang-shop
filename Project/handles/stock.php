<?php
// ดึงข้อมูลสินค้าทั้งหมดออกมา
function get_products() {
    $query = 'select * from product';
    return database_select_all($query);
}

// ดึงข้อมูลสินค้าที่ขายดีที่สุด 10 ข้อมูลออกมา
function get_select_product_top() {
    $query = <<< eot
    select 
	product.prod_id,
        product.prod_name,
        sum(detail_amount) as amount
    from orders_detail inner join product 
	on orders_detail.prod_id = product.prod_id
	group by prod_id
	order by detail_amount desc 
        limit 0, 10 
eot;
    return database_select_all($query);
}

// ดึงข้อมูลสินค้าที่ขายไปแล้ว
function get_select_product_sale() {
    $query = <<< eot
    select 
	product.prod_id,
        product.prod_name,
        sum(detail_amount) as amount,
        sum(detail_price) as price
    from orders_detail 
        inner join product 
            on orders_detail.prod_id = product.prod_id
        inner join orders
            on orders_detail.ord_id = orders.ord_id
        where ord_status = 'success'
	group by prod_id
eot;
    return database_select_all($query);
}

// ดึงข้อมูลสินค้าที่ขายไม่ได้เลยออกมา
function get_select_product_no_sale() {
    $query = <<< eot
        select 
            product.prod_id,
            prod_name,
            prod_amount as amount
        from 
        product left join orders_detail
        on product.prod_id = orders_detail.prod_id
where orders_detail.detail_amount is null;
eot;
    return database_select_all($query);
}

// ดึงข้อมูลสินค้าที่ขายหมดแล้ว
function get_select_product_losed() {
    $query = <<< eot
        select 
            prod_id,
            prod_name,
            (select sum(detail_amount) from orders_detail where orders_detail.prod_id = product.prod_id) as amount
        from product where prod_amount <= 0
eot;
    return database_select_all($query);
}
