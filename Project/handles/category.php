<?php

// บันทึกข้อมูลประเภทสินค้า
if (get_input('cate_save') === 'create') {
    $cate_name = get_input('cate_name');
    if (database_query("insert into category set cate_name='$cate_name'") > 0) {
        redirect(get_site('category'), false);
    }
    alert_error('ไม่สามารถบันทึกข้อมูลได้', database_error());
}

// แก้ไขข้อมูลประเภทสินค้า
if (get_input('cate_save') === 'update') {
    $cate_name = get_input('cate_name');
    $cate_id = get_input('cate_id');
    if (database_query("update category set cate_name='$cate_name', cate_created=now() where cate_id=$cate_id")) {
        redirect(get_site('category'), false);
    }
    alert_error('ไม่สามารถแก้ไขข้อมูลได้', database_error(), get_site('category'));
}

// ลบข้อมูลประเภทสินค้า
if (get_input('cate_delete')) {
    $cate_id = get_input('cate_delete');
    if (database_query("delete from category where cate_id=$cate_id")) {
        redirect(get_site('category'), false);
    }
    alert_error('ไม่สามารถลบข้อมูลได้', database_error());
}

// ดึงข้อมูลจากราตาง category
function get_categories() {
    return get_pagination('select * from category order by cate_created desc', PAGE_MAX);
}

// ดังข้อมูลจากตาราง category ผ่าน cate_id
function get_category_by_id($cate_id) {
    if (!empty($cate_id)) {
        return database_select_row("select * from category where cate_id=$cate_id");
    }
}
