<?php

// update สถานะการสั่งซื้อ [ ord_status ]
if (!empty($status = get_input('ord_status')) && !empty($ord_id = get_input('ord_id'))) {
    database_query("update orders set ord_status = '$status' where ord_id = $ord_id");
    redirect(get_site('order'), false);
}

// update สถานะการส่งสินค้า [ ship_status ]
if (!empty($status = get_input('ship_status')) && !empty($ord_id = get_input('ord_id'))) {
    database_query("update orders set ship_status = '$status' where ord_id = $ord_id");
    redirect(get_site('order'), false);
}

// update สถานะการชำระเงิน [ pay_status ]
if (!empty($status = get_input('pay_status')) && !empty($ord_id = get_input('ord_id'))) {
    database_query("update orders set pay_status = '$status' where ord_id = $ord_id");
    redirect(get_site('order'), false);
}

// ดึงข้อมูลการสั่งซื้อสินค้าทั้งหมด
function get_orders() {
    $query = "
        select 
            orders.*,
            payment.*,
            orders.ord_id
        from 
            orders left join payment 
            on orders.ord_id = payment.ord_id 
        order by 
            orders.ord_date desc";
    $orders = database_select_all($query);
    $orders_return = [];

    if (count($orders) > 0) {
        foreach ($orders as $item) {
            $ord_id = $item['ord_id'];
            $item['details'] = database_select_all("select * from orders_detail where ord_id = $ord_id");
            $orders_return[] = $item;
        }
    }

    return $orders_return;
}