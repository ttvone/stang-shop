<?php include './configs/autoload.php'; ?>
<?php include './handles/payment.php'; ?>
<!DOCTYPE HTML>
<html>
    <head>
        <title></title>
        <?php load('styles') ?>
    </head>
    <body>
        <div class="wrap">
            <div class="container">
                <?php load('header') ?>
                <div class="content">
                    <div class="content_box">
                        <?php load('pages/payment') ?>
                        <?php load('footer') ?>
                    </div>
                </div>
            </div>
        </div>
        <?php load('scripts') ?>
    </body>
</html>
