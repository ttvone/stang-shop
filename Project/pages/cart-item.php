<div class="form-group">
    <h3>
        <i class="glyphicon glyphicon-shopping-cart"></i>
        ตะกร้าสินค้าของฉัน
    </h3>
</div>
<div class="form-group">
    <h4>รายการข้อมูลสินค้าที่คุณได้เลือกไว้</h4>
    <br>
    <div class="table-responsive">
        <table class="table cart-table table-striped">
            <thead>
                <tr>
                    <th style="width: 120px">สินค้า</th>
                    <th>ชื่อสินค้า</th>
                    <th>ราคาสินค้า</th>
                    <th>จำนวนที่สั่ง</th>
                    <th>ราคารวม</th>
                    <th>นำออกจากตะกร้า</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $total_all = 0;
                $products = get_product_by_store();
                ?>
                <?php if (count($products) === 0) : ?>
                    <tr>
                        <td colspan="5">ไม่มีข้อมูลสินค้าในตะกร้าสินค้า</td>
                    </tr>
                <?php else: ?>
                    <?php foreach (get_product_by_store() as $item) : ?>
                        <?php
                        $store = search_object(cart_store(), 'prod_id', $item['prod_id']);
                        $amount = (!empty($store)) ? $store['amount'] : 0;
                        $total = ($amount * $item['prod_price']);
                        $total_all += $total;
                        ?>
                        <tr>
                            <td>
                                <a href="" class="cart-image img-thumbnail"
                                   style="background-image: url(<?= get_url("/images?prod_id={$item['prod_id']}") ?>)"></a>
                                <p class="text-muted">
                                    <small>รหัสสินค้า : <?= $item['prod_id'] ?></small>
                                </p>
                            </td>
                            <td>
                                <?= $item['prod_name'] ?>
                                <p>
                                    <small>เหลือ <?= $item['prod_amount'] ?> ชื้น</small>
                                </p>
                            </td>
                            <td>
                                <?= number_format($item['prod_price']) ?> บาท
                            </td>
                            <td>
                                <form method="post" action="<?= get_url('/cart.php') ?>">
                                    <input type="hidden" name="product_edit_cart" value="<?= $item['prod_id'] ?>" />
                                    <div class="input-group cart-edit" style="width: 100px">
                                        <input type="text" class="form-control" name="amount" 
                                               data-amount="<?= $item['prod_amount'] ?>" 
                                               data-value="<?= $amount ?>" 
                                               value="<?= $amount ?>" 
                                               onfocus="cart_focus_product_amount(this, true)" 
                                               onblur="cart_focus_product_amount(this, false)">
                                        <span class="input-group-addon">ชิ้น</span>
                                    </div>
                                </form>
                            </td>
                            <td>
                                <?= number_format($total) ?> บาท
                            </td>
                            <td>
                                <a href="<?= get_url('/cart.php', ['product_delete_cart' => $item['prod_id']]) ?>" class="text-danger" confirm="ต้องการนำสินค้านี้ออกจากตะกร้าจริงหรือ?">
                                    <u>นำออกจากตะกร้า</u>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="6" class="text-right">
                        <div class="form-group">
                            ราคารวมทั้งหมด <?= number_format($total_all) ?> บาท
                        </div>
                        <div class="form-group">
                            <a href="<?= get_url('/') ?>" class="btn btn-default">
                                <i class="glyphicon glyphicon-th"></i> กลับไปที่หน้าสินค้า
                            </a>
                            <a href="<?= get_url('/cart.php', ['product_clear_cart' => true]) ?>" class="btn btn-danger" confirm="ต้องการลบสินค้าทั้งหมดจริงหรือ?">
                                <i class="glyphicon glyphicon-trash"></i> ลบสินค้าทั้งหมด
                            </a>
                            <a href="<?= get_site('shipping') ?>" class="btn btn-info">
                                <i class="glyphicon glyphicon-ok-sign"></i> ยืนยันการส่งซื้อ
                            </a>
                        </div>
                    </th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>