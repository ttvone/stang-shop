<div class="men">
    <div class="account-in">
        <h2>
            แจ้งชำระเงินค่าสินค้า <br>
            <small>กรอกข้อมูลการชำระเงินของคุณเพื่อแจ้งให้ทางเราทราบ</small>
        </h2>
        <div class="col-md-6 account-top">
            <form method="post" autocomplete="off" class="payment-form">
                <div> 	
                    <span>หมายเลขสั่งซื้อ</span>
                    <input type="text" class="input" required="" name="ord_id"> 
                </div>
                <div> 
                    <span>วันที่</span>
                    <input type="date" class="input" required="" name="pay_date">
                </div>	
                <div> 
                    <span>เวลา</span>
                    <input type="time" class="input" required="" name="pay_time">
                </div>		
                <div> 
                    <span>จำนวนเงิน</span>
                    <input type="text" pattern="^[\d.]+$" class="input" required="" name="pay_amount">
                </div>			
                <input type="submit" value="แจ้งชำระเงิน" name="pay_submit"> 
                |
                <a href="<?= get_url('/') ?>"><u>กลับไปหน้าแรก</u></a>
            </form>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>