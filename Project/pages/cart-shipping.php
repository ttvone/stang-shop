<?php
$order_price = get_cart_amount(true);
$total = $order_price['total'];
$tax = $order_price['tax'];
$shipping = $order_price['shipping'];
?>
<div class="form-group">
    <h3>
        <i class="glyphicon glyphicon-send"></i>
        กรอกข้อมูลเพื่อจัดส่งสินค้า
    </h3>
    <div class="alert alert-warning">
        <p>
            ราคาสินค้าในตระกร้าที่คุณซื้อไปทั้งสินค้า <?= $order_price['count'] ?> ชิ้น รายการที่ต้องจ่ายทั้งสิ้น :
        </p>
        <br>
        <table>
            <tr>
                <td>ค่าสินค้าที่ซื้อ</td>
                <td>: <?= number_format($total, 2) ?> บาท</td>
            </tr>
            <!-- <tr>
                <td>ค่าภาษีมูลค่าเพิ่ม 7%</td>
                <td>: <?= number_format($tax, 2) ?> บาท</td>
            </tr> -->
            <tr>
                <td>ค่าส่งสินค้า</td>
                <td>: <?= number_format($shipping, 2) ?> บาท</td>
            </tr>
            <tr><td colspan="2">&nbsp</td></tr>
            <tr>
                <th>รวมเป็นเงินที่ต้องจ่ายทั้งสิ้น &nbsp;&nbsp;&nbsp;</th>
                <th>: <?= number_format(($total + $tax + $shipping), 2) ?> บาท</th>
            </tr>
        </table>
    </div>
</div>

<form class="input-form" method="post">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label>ชื่อผู้รับ :</label>
                <input type="text" class="input" name="ship_name" required=""/>
            </div>

            <div class="form-group">
                <label>อีเมล์ :</label>
                <input type="email" class="input" name="ship_email" required=""/>
            </div>

            <div class="form-group">
                <label>เบอร์โทร :</label>
                <input type="number" class="input" name="ship_phone" required=""/>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group">
                <label>ที่อยู่ :</label>
                <textarea class="input" style="height: 220px" name="ship_address" required=""></textarea>
            </div>
        </div>
    </div>


    <div class="form-group" style="margin-top: 15px;">
        <a href="<?= get_url('/cart.php') ?>" class="btn btn-default button button-min">
            <i class="glyphicon glyphicon-chevron-left"></i> ย้อนกลับ
        </a>

        <button href="" class="btn btn-info button button-min" name="shipping_button" value="send">
            <i class="glyphicon glyphicon-send"></i> ส่งข้อมูล
        </button>
    </div>
</form>