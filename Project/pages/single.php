<?php
insert_view_count_product(set_value('prod_id', 0));
$product = get_product(set_value('prod_id', 0));
if (empty($product)) {
    redirect('/');
}
$product_images = get_product_images($product['prod_id']);
?>
<div class="men">
    <div class="single_top">
        <div class="col-md-9 single_right">
            <div class="grid images_3_of_2">
                <div class="form-group">
                    <ul id="etalage">
                        <?php foreach ($product_images as $item) : ?>
                            <?php $image_id = $item['image_id'] ?>
                            <li>
                                <img class="etalage_thumb_image img-responsive" src="<?= get_url("/images?id=$image_id") ?>" />
                                <img class="etalage_source_image img-responsive" src="<?= get_url("/images?id=$image_id") ?>" />
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <p>
                    จำนวนรับชมสินค้า <?= $product['prod_view_count'] ?> ครั้ง
                </p>
                <p>
                    <small>วันที่อัพเดต: <?= $product['prod_created'] ?></small>
                </p>
            </div>
            <div class="desc1 span_3_of_2">
                <h1>ชื่อสินค้า: <?= $product['prod_name'] ?></h1>
                <p class="m_5 form-group">
                    ราคา <?= number_format($product['prod_price'], 2) ?> บาท
                </p>
                <p class="form-group">จำนวนที่เหลือ: <?= $product['prod_amount'] ?> ชื้น</p>
                <p class="form-group">ประเภทสินค้า: <?= $product['cate_name'] ?></p>
                <div class="btn_form">
                    <?php if ($product['prod_amount'] <= 0) : ?>
                        <button class="btn-button-form" style="cursor: no-drop; opacity: .45;">
                            <i class="glyphicon glyphicon-ban-circle"></i> สินค้าหมด!!!
                        </button>
                    <?php else: ?>
                        <a href="<?= get_product_into_cart($product['prod_id']) ?>" class="btn-button-form">
                            <i class="glyphicon glyphicon-shopping-cart"></i> หยิบใส่ตะกร้า
                        </a>
                    <?php endif; ?>
                    <span>|</span>
                    <a href="<?= get_url() ?>">
                        <small><u>กลับไปหน้าสินค้า</u></small>
                    </a>
                </div>
                <div class="form-group">
                    <span class="m_link">
                        รายละเอียดสินค้า:
                    </span>
                    <p class="m_text2">
                        <span class="block">
                            <?= $product['prod_detail'] ?: 'ไม่มีรายละเอียด !' ?>
                        </span>
                    </p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-3">
            <!--FlexSlider-->
            <section class="slider_flex form-group">
                <div class="flexslider">
                    <ul class="slides">
                        <?php foreach ($product_images as $item) : ?>
                            <li>
                                <img src="<?= get_url("/images?id={$item['image_id']}") ?>"
                                     class="product-single-image"
                                     alt="<?= $item['image_name'] ?>"/>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </section>
            <!--FlexSlider-->
            <a href="<?= get_back() ?>" class="btn btn-success btn-block no-radius">
                <i class="glyphicon glyphicon-chevron-left"></i> กลับไปหน้าที่แล้ว
            </a>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
