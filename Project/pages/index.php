<div class="men">

    <!--ด้านข้างสำหรับดูสินค้าจากตัวเลือกอื่นๆ-->
    <div class="col-md-3 sidebar">
        <div class="block block-layered-nav">
            <div class="block-title">
                <strong>
                    <b><i class="glyphicon glyphicon-th-list"></i> เลือกดูรายการสินค้า</b>
                </strong>
            </div>
            <div class="block-content">

                <dl id="narrow-by-list">

                    <dt class="odd">จากราคาสินค้า</dt>
                    <dd class="odd">
                        <ol>
                            <?php foreach (get_category_prices(10) as $item) : ?>
                                <li>
                                    <a href="<?= get_url('/', ['price_form' => $item[0], 'price_to' => $item[1]]) ?>">
                                        <span class="price1">ราคา <?= number_format($item[0], 0) ?></span> 
                                        - 
                                        <span class="price1"><?= number_format($item[1], 0) ?> บาท</span>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ol>
                    </dd>

                    <dt class="even">จากหมวดหมู่สินค้า</dt>
                    <dd class="even">
                        <ol>
                            <?php foreach (get_categories() as $item) : ?>
                                <li>
                                    <a href="<?= get_url('/', ['cate_id' => $item['cate_id']]) ?>"><?= $item['cate_name'] ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ol>
                    </dd>

                    <dt class="last odd">จากสินค้าที่มาใหม่</dt>
                    <dd class="last odd">
                        <ol>
                            <?php foreach (get_new_products() as $item) : ?>
                                <li>
                                    <a href="<?= get_url('/single.php?prod_id=' . $item['prod_id']) ?>"><?= $item['prod_name'] ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ol>
                    </dd>

                    <dt class="last odd">จากสินค้าที่อัพเดตล่าสุด</dt>
                    <dd class="last odd">
                        <ol>
                            <?php foreach (get_last_products() as $item) : ?>
                                <li>
                                    <a href="<?= get_url('/single.php?prod_id=' . $item['prod_id']) ?>"><?= $item['prod_name'] ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ol>
                    </dd>
                </dl>

            </div>
        </div>
    </div>

    <!--ด้านแสดงสินค้าหลัก-->
    <div class="col-md-9">
        <div class="mens-toolbar">
            <div class="sort">
                <div class="sort-by">
                    <label>
                        <span class="glyphicon glyphicon-tags" style="margin-right: 3px;"></span>
                        รายการสินค้าของเรา
                    </label>
                </div>
            </div>
            <div class="pager">   
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="span_2">
            <?php $products = get_product_dashboard() ?>
            <?php foreach ($products['items'] as $item) : ?>
                <?php $prod_id = $item['prod_id'] ?>
                <div class="col_1_of_single1 span_1_of_single1">
                    <a href="<?= get_url("/single.php?prod_id=$prod_id") ?>">
                        <div class="product-image img" style="background-image: url(<?= get_url("/images?prod_id=$prod_id") ?>)"></div>

                        <h3 class="text-overflow">
                            <?= $item['prod_name'] ?>
                        </h3>
                        <p class="text-overflow">
                            <?= $item['cate_name'] ?> (เหลือ <?= $item['prod_amount'] ?> ชิ้น)
                        </p>
                        <h4>
                            ฿<?= number_format($item['prod_price'], 2) ?>
                            <?php if ($item['prod_amount'] <= 0) : ?>
                                <button class="btn btn-success btn-sm" disabled="">
                                    <i class="glyphicon glyphicon-ban-circle"></i> สินค้าหมด!!!
                                </button>
                            <?php else : ?>
                                <button class="btn btn-success btn-sm" onclick="location.href = '<?= get_product_into_cart($prod_id) ?>'; return false;">
                                    <i class="glyphicon glyphicon-shopping-cart"></i> ใส่ตะกร้า
                                </button>
                            <?php endif; ?>
                        </h4>
                    </a>  
                </div> 
            <?php endforeach; ?>
            <div class="clearfix"></div>
            <div class="text-center">
                <?= pagination_template(null, $products) ?>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>