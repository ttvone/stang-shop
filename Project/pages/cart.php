<div class="men cart">
    <?php
    switch (get_input('site')) {
        case 'shipping':
            // ถ้าไม่มีสินค้าในตะกร้าห้ามเข้าหน้านี้ ให้กลับไปที่หน้า cart.php
            if (count(cart_store()) == 0) {
                redirect(get_url('/cart.php'), false);
            }
            load('cart-shipping');
            break;
        case 'order-invoice':
            load('cart-invoice');
            break;
        default:
            load('cart-item');
            break;
    }
    ?>
</div>
