<div class="men">
    <div class="account-in">
        <h2>
            เข้าสู่ระบบ
            <small>* สำหรับผู้ดูแลระบบเท่านั้น</small>
        </h2>
        <div class="col-md-7 account-top">
            <form method="post" autocomplete="off">
                <div> 	
                    <span>ชื่อผู้ใช้งาน*</span>
                    <input type="text" required="" name="admin_user" value="<?= set_value('admin_user') ?>"> 
                </div>
                <div> 
                    <span class="pass">รหัสผ่าน*</span>
                    <input type="password" required="" name="admin_password">
                </div>				
                <input type="submit" value="เข้าสู่ระบบ" name="submit"> 
            </form>
        </div>
        <div class="col-md-5 left-account ">
            <a href="<?= get_url('/single.php') ?>"><img class="img-responsive " src="<?= get_url('/assets/images/s4.jpg') ?>" alt=""></a>
            <div class="five-in">
                <h1>25% </h1><span>ลดราคา</span>
            </div>
            <a href="" class="create">ไปที่หน้าสินค้าของเรา</a>
            <div class="clearfix"> </div>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>