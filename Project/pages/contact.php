<div class="men">
    <h1 class="contact_head">Our Address</h1>
    <div class="contact_box">
        <div class="col-sm-4">
            <address class="addr">
                <p>
                    8901 Marmora Road, <br>
                    Glasgow, D04 89GR.
                </p>
                <dl>
                    <dt>Freephone:</dt>
                    <dd>+1 800 559 6580</dd>
                </dl>
                <dl>
                    <dt>Telephone:</dt>
                    <dd>+1 800 603 6035</dd>
                </dl>
                <dl>
                    <dt>FAX:</dt>
                    <dd>+1 800 889 9898</dd>
                </dl>
                <p>E-mail:
                    <a href="#"> mail@demolink.org</a>
                </p>
            </address>
        </div>
        <div class="col-sm-4">
            <address class="addr">
                <p>
                    8901 Marmora Road, <br>
                    Glasgow, D04 89GR.
                </p>
                <dl>
                    <dt>Freephone:</dt>
                    <dd>+1 800 559 6580</dd>
                </dl>
                <dl>
                    <dt>Telephone:</dt>
                    <dd>+1 800 603 6035</dd>
                </dl>
                <dl>
                    <dt>FAX:</dt>
                    <dd>+1 800 889 9898</dd>
                </dl>
                <p>E-mail:
                    <a href="#"> mail@demolink.org</a>
                </p>
            </address>
        </div>
        <div class="col-sm-4">
            <address class="addr">
                <p>
                    8901 Marmora Road, <br>
                    Glasgow, D04 89GR.
                </p>
                <dl>
                    <dt>Freephone:</dt>
                    <dd>+1 800 559 6580</dd>
                </dl>
                <dl>
                    <dt>Telephone:</dt>
                    <dd>+1 800 603 6035</dd>
                </dl>
                <dl>
                    <dt>FAX:</dt>
                    <dd>+1 800 889 9898</dd>
                </dl>
                <p>E-mail:
                    <a href="#"> mail@demolink.org</a>
                </p>
            </address>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
