<div class="men">
    <h3>วิธีการสั่งซื้อสินค้า</h3>
    <p>คุณสามารถดูวิดีโอสอนการสั่งซื้อสินค้าจากเว็บไซต์ของเราได้</p>
    <hr>
    <div class="form-group">
        <iframe src="https://player.vimeo.com/video/210617216" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    </div>
    <div class="form-group">
        <a href="<?= get_url('/') ?>" class="btn btn-default button">
            กลับไปยังหน้าแรก
        </a>
    </div>
</div>
