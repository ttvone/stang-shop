<?php
$orders = get_orders_by_ord_number(set_value('id', 0));
if (empty($orders)) {
    redirect(get_url('/'));
}
$order = $orders['order'];
$detail = $orders['detail'];
?>
<div class="form-group">
    <h3>
        <i class="glyphicon glyphicon-list-alt"></i>
        รายการสั่งซื้อเลขที่ <?= str_pad($order['ord_id'], 5, '0', STR_PAD_LEFT) ?>
    </h3>
</div>
<div class="alert alert-warning hidden-print">
    * กรุณาเก็บหมายเลขสั่งซื้อไว้เพื่อตรวจสอนสินค้าภายหลัง
</div>
<div class="well" style="min-width: 700px;">
    <div class="form-group">
        <div class="row">
            <div class="col-xs-6">
                <table class="order-table">
                    <tr>
                        <td>หมายเลขสั่งซื้อ</td>
                        <td>
                            : <u><?= $order['ord_number'] ?></u>
                            <small class="text-muted    ">(เก็บส่วนนี้ไว้)</small>
                        </td>
                    </tr>
                    <tr>
                        <td>สถานะการสั่งซื้อ</td>
                        <td>: <?= get_order_status($order['ord_status']) ?></td>
                    </tr>
                    <tr>
                        <td>ที่อยู่อีเมล์</td>
                        <td>: <?= $order['ship_email'] ?></td>
                    </tr>
                    <tr>
                        <td>เบอร์โทร</td>
                        <td>: <?= $order['ship_phone'] ?></td>
                    </tr>
                </table>
            </div>
            <div class="col-xs-6">
                <table class="order-table">
                    <tr>
                        <td width="140">วันที่สั่งซื้อสินค้า</td>
                        <td>: <?= $order['ord_date'] ?></td>
                    </tr>
                    <tr>
                        <td>ที่อยู่ที่จัดส่งสินค้า</td>
                        <td>: <?= $order['ship_address'] ?></td>
                    </tr>
                    <!-- <tr>
                        <td>สถานะการจัดส่ง</td>
                        <td>: <?= get_order_status($order['ship_status'], 'shipping') ?></td>
                    </tr>
                    <tr>
                        <td>สถานะการชำระเงิน</td>
                        <td>: <?= get_order_status($order['pay_status'], 'payment') ?></td>
                    </tr> -->
                </table>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>ชื่อสินค้า</th>
                        <th>จำนวนสินค้า</th>
                        <th>ราคา</th>
                        <th>ราคารวม</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $total = 0;
                    ?>
                    <?php foreach ($detail as $item) : ?>
                        <?php
                        $total_price = $item['detail_price'] * $item['detail_amount'];
                        $total += $total_price;
                        ?>
                        <tr>
                            <td>
                                <a href="<?= get_url('/single.php?prod_id=' . $item['prod_id']) ?>" class="hidden-print">
                                    <u><?= $item['prod_name'] ?></u>
                                </a>
                                <u class="visible-print-inline"><?= $item['prod_name'] ?></u>
                            </td>
                            <td><?= $item['detail_amount'] ?> ชื้น</td>
                            <td><?= number_format($item['detail_price'], 2) ?> บาท</td>
                            <td><?= number_format($total_price, 2) ?> บาท</td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="3" class="text-right">ราคารวม</th>
                        <td colspan="1"><?= number_format($total, 2) ?> บาท</td>
                    </tr>
                    <tr>
                        <th colspan="3" class="text-right">ค่าขนส่งสินค้า</th>
                        <td colspan="1"><?= number_format($order['ship_price'], 2) ?> บาท</td>
                    </tr>
                    <tr>
                        <th colspan="3" class="text-right">ราคาที่ต้องจ่ายทั้งสิ้น</th>
                        <td colspan="1"><?= number_format($total + $order['ship_price'], 2) ?> บาท</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<div class="row hidden-print">
    <div class="col-sm-6 text-left">
        <a href="<?= get_url('/') ?>" class="btn btn-default button"><i class="glyphicon glyphicon-home"></i> กลับไปที่หน้าแรก</a>
        <a href="<?= get_back() ?>" class="btn btn-default button"><i class="glyphicon glyphicon-chevron-left"></i> กลับไปหน้าที่แล้ว</a>
    </div>
    <div class="col-sm-6 text-right">
        <button class="btn btn-default button" onclick="print()">
            <i class="glyphicon glyphicon-print"></i> พิมพ์รายการสั่งซ์้อนี้
        </button>
    </div>
</div>
