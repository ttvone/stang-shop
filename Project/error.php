<?php header('Content-Type: text/html; charset=utf-8', true, 500) ?>
<?php include './configs/autoload.php'; ?>
<!DOCTYPE HTML>
<html>
    <head>
        <title></title>
        <?php load('styles') ?>
    </head>
    <body>
        <div class="wrap">
            <div class="container">
                <?php load('header', 'contact') ?>
                <div class="content">
                    <div class="content_box">
                        <div class="men">
                            <div class="error-404 text-center">
                                <h1><?= get_input('title') ?: '404' ?></h1>
                                <p><?= get_input('content') ?: 'File not found.' ?></p>
                                <!--Redirect to forward page-->
                                <?php if (get_input('goback') === 'ref') : ?>
                                    <a class="b-home" href="<?= $_SERVER['HTTP_REFERER'] ?>">
                                        ย้อนกลับไปหน้าที่แล้ว
                                    </a>
                                <?php elseif (!empty(get_input('goback'))) : ?>
                                    <a class="b-home" href="<?= get_input('goback') ?>">
                                        ย้อนกลับไปหน้าที่แล้ว
                                    </a>
                                <?php else: ?>
                                    <a class="b-home" href="index.php" onclick="history.back(); return false;">
                                        ย้อนกลับไปหน้าที่แล้ว
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php load('footer') ?>
                    </div>
                </div>
            </div>
        </div>
        <?php load('scripts') ?>
    </body>
</html>
