<?php include './configs/autoload.php'; ?>
<?php load('handles/invoice') ?>
<?php $order = get_invoice() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ใบเสร็จรับเงิน ห้องเสื้อสตางค์</title>
    <style>
        * {
            padding: 0;
            margin: 0;
            box-sizing: border-box;
        }
        html, body {
            font-family: sans-serif;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }
        body {
            padding: 2em 0;
        }
        .container {
            width: 600px;
            margin: auto;
        }
        .items-table {
            width: 100%;
            border-collapse: collapse;
        }
        .items-table th,
        .items-table td {
            padding: 5px 15px;
            text-align: left;
            border-left: solid 1px black;
        }
        .items-table th:last-child,
        .items-table td:last-child {
            border-right:  solid 1px black;
        }
        .items-table tfoot tr:first-child td {
            border-top: solid 1px black;
        }
        .items-table th,
        .items-table tfoot tr:last-child td {
            border-bottom: solid 1px black;
        }
        .items-table tfoot tr td:nth-child(1),
        .items-table tfoot tr td:nth-child(2) {
            border-left: none;
            border-bottom: none;
        }
        .form-table {
            width: 100%;
            border-collapse: collapse;
            border: solid 1px black;
        }
        .form-table td {
            padding: 15px;
            vertical-align: top;
        }
        .form-table td:first-child {
            width: 260px;
            padding-left: 15px;
            padding-top: 15px;
        }
        .form-table td > p {
            line-height: 2.5em;
        }
        .form-table div {
            padding: 15px;
            border: solid 3px black;
            font-size: 10pt;
            text-align: center; 
            margin-bottom: 15px; 
        }
        .form-table div h1 {
            font-size: 14pt;
            margin-bottom: 7px;
        }
        .form-table h2 {
            margin-bottom: 20px;
            font-size: 20pt;
        }
        @media print {
            @page {
                size: auto;   /* auto is the initial value */
                margin: 0 50px;  /* this affects the margin in the printer settings */
            }
        }
    </style>
</head>
<body>
    <div class="container">
        <table class="form-table">
            <tr>
                <td>
                    <div>
                        <h1>ห้องเสื้อสตางค์</h1>
                        <p>199/102 หมู่ 17 ตำบลในเมือง อำเภอเมือง จังหวัดขอนแก่น 4000 โทร 087-946-2223</p>
                    </div>
                    <p><strong>ที่อยู่:</strong> <?= $order['ship_address'] ?></p>
                </td>
                <td>
                    <h2>ใบเสร็จรับเงิน</h2>
                    <p><strong>เลขที่:</strong>  <?= str_replace('-', '', $order['pay_date']) . '/' . $order['ord_id'] ?></p>
                    <p>วันที่:   <?= $order['ord_date'] ?></p>
                    <p>ชื่อ:   <?= $order['ship_name'] ?></p>
                    <p>เลขที่ใบสั่งซื้อ: <?= $order['ord_number'] ?></p>
                </td>
            </tr>
        </table>
        <table class="items-table">

            <thead>
                <tr class="border-top border-bottom">
                    <th style="width: 17%;">รหัสสินค้า</th>
                    <th>รายการ</th>
                    <th style="width: 17%;">จำนวน</th>
                    <th style="width: 20%;">ราคา</th>
                </tr>
            </thead>

            <tbody>
                <?php 
                    $prices = 0; 
                    $total = 0;
                ?>
                <?php foreach ( $order['details'] as $item ) : ?>
                    <?php
                        $amount = $item['detail_amount'];
                        $price  = $item['detail_price'] * $amount;
                        $prices = $prices + $price;
                    ?>
                    <tr>
                        <td><?= str_pad($item['prod_id'], 4, '0', STR_PAD_LEFT) ?></td>
                        <td><?= $item['prod_name'] ?></td>
                        <td><?= number_format($amount, 2) ?></td>
                        <td><?= number_format($price, 2) ?></td>
                    </tr>
                <?php endforeach; ?>

                <?php for($i = 0; $i < 2; $i++) : ?>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                <?php endfor; ?>
            </tbody>

            <tfoot>
                <tr>
                    <td></td>
                    <td></td>
                    <td><strong>รวม</strong></td>
                    <td><?= number_format($prices, 2) ?></td>
                </tr>

                <tr>
                    <td></td>
                    <td></td>
                    <td><strong>จัดส่ง</strong></td>
                    <td><?= number_format($order['ship_price'], 2) ?></td>
                </tr>

                <tr>
                    <td></td>
                    <td></td>
                    <td><strong>รวมทั้งสิ้น</strong></td>
                    <td><?= number_format($order['ship_price'] + $prices, 2) ?></td>
                </tr>
            </tfoot>

        </table>
    </div>
</body>
</html>